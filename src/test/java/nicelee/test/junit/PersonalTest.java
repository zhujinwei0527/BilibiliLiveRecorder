package nicelee.test.junit;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;

/**
 * @Author Taylor Zhu
 * @Date: 2024/2/23
 * @Description:nicelee.test.junit
 * @version: 1.0
 */

public class PersonalTest {
    @Test
    public void test1() throws IOException {
        String url = "https://www.google.com/";
        String gateIp = "milan.wuyadesiji.eu.org";
        int gatePort = 29775;
        Proxy proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(gateIp, gatePort));
        java.net.Authenticator.setDefault(new java.net.Authenticator() {
            private PasswordAuthentication authentication =
                    new PasswordAuthentication("8Y4yCZ5Olu", "FYl1ve4B94".toCharArray());

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return authentication;
            }
        });
        OkHttpClient client = new OkHttpClient().newBuilder().proxy(proxy).build();

        Request request = new Request.Builder().url(url).build();
        okhttp3.Response response = client.newCall(request).execute();
        String responseString = response.body().string();
        System.out.println(responseString);
        response.close();
    }
}
