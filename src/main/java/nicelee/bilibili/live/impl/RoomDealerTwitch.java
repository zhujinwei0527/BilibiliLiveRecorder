package nicelee.bilibili.live.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import nicelee.bilibili.live.RoomDealer;
import nicelee.bilibili.live.domain.RoomInfo;
import nicelee.bilibili.util.Logger;
import okhttp3.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * twitch 录制
 */
public class RoomDealerTwitch extends RoomDealer {

    final public static String liver = "twitch";

    @Override
    public String getType() {
        return ".flv";
    }

    @Override
    public RoomInfo getRoomInfo(String shortId) {
        String url = "https://api.twitch.tv/helix/search/channels?query=" + shortId + "&first=1";
        String bearerToken = "vzpf17b83ur978it2iimfjc5x41826";
        String clientId = "1dq8mjfsv44qwo3ddna2h7p8hzg9al";

        /* 使用代理获取链接====start======*/
        String gateIp = "milan.wuyadesiji.eu.org";
        int gatePort = 29775;
        Proxy proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(gateIp, gatePort));
        java.net.Authenticator.setDefault(new java.net.Authenticator() {
            private PasswordAuthentication authentication =
                    new PasswordAuthentication("8Y4yCZ5Olu", "FYl1ve4B94".toCharArray());

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return authentication;
            }
        });
        OkHttpClient client = new OkHttpClient().newBuilder().proxy(proxy).build();
        /* 使用代理获取链接=======end========*/

//        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer " + bearerToken)
                .addHeader("Client-Id", clientId)
                .build();
        Response response = null;
        String responseBody = "";
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                responseBody = response.body().string();
                System.out.println("Response: " + responseBody);
            } else {
                Logger.println("Request failed");
            }
        } catch (IOException e){
            Logger.println("请求失败");
            e.printStackTrace();
        } finally {
            response.close();
        }

        JSONObject jsonObject = JSON.parseObject(responseBody);
        JSONArray dataArray = jsonObject.getJSONArray("data");
        JSONObject data = null;
        if (dataArray != null && !dataArray.isEmpty()) {
            data = dataArray.getJSONObject(0);
            // 在这里你可以进一步处理 data 对象
            Logger.println("data[]" + data);
        } else {
            Logger.println("获取房间信息返回null");
        }

//        JSONObject data = jsonObject.getJSONObject("data");
        String roomId = data.getString("broadcaster_login");
        data.getString("display_name");
        long userId = data.getLong("id");
        String userName = data.getString("display_name");
        boolean liveStatus = data.getBoolean("is_live");
        String title = data.getString("title");
        String description = data.getString("title");
        String[] acceptQuality = new String[1];
        String[] acceptQualityDesc = new String[1];
        acceptQuality[0] = "10000";
        acceptQualityDesc[0] = "超清";

        RoomInfo roomInfo = new RoomInfo();
        roomInfo.setShortId(shortId);
        roomInfo.setRoomId(roomId);
        roomInfo.setUserId(userId);
        roomInfo.setUserName(userName);
        roomInfo.setAcceptQuality(acceptQuality);
        roomInfo.setAcceptQualityDesc(acceptQualityDesc);
        if (liveStatus){
            roomInfo.setLiveStatus(1);
        } else {
            roomInfo.setLiveStatus(0);
        }
        roomInfo.setTitle(title);
        roomInfo.setDescription(description);
        return roomInfo;
    }

    @Override
    public String getLiveUrl(String shortId, String qn, Object... obj) {

        String[] sigToken = new String[2];
        try {
            sigToken = getSigToken(shortId);
        } catch (IOException e){
            System.out.println("获取直播信息失败");
            e.printStackTrace();
        }
        String signature = sigToken[0];
        String token = sigToken[1];
        Map<String, String> params = new HashMap<>();
        params.put("allow_source", "true");
        params.put("dt", "2");
        params.put("fast_bread", "true");
        params.put("player_backend", "mediaplayer");
        params.put("playlist_include_framerate", "true");
        params.put("reassignments_supported", "true");
        params.put("sig", signature);
        params.put("supported_codecs", "vp09,avc1");
        params.put("token", token);
        params.put("cdm", "wv");
        params.put("player_version", "1.4.0");
        String queryString = encodeParams(params);
        return "https://usher.ttvnw.net/api/channel/hls/" + shortId + ".m3u8?" + queryString;
    }

    @Override
    public void startRecord(String url, String fileName, String shortId) {
        util.download(url, fileName + ".flv", headers.getTwitchHeaders());
    }

    /**
     * 获取twitch直播的客户端id
     * @param rid 房间名
     * @return
     * @throws IOException
     */
    private String getClientId(String rid) throws IOException {
        String url = "https://www.twitch.tv/" + rid;
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headerMap.put("Accept-Encoding", "gzip");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8");
        headerMap.put("Cache-Control", "max-age=0");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Host", "www.twitch.tv");
        headerMap.put("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0");
        String res = util.getContent(url, headerMap, null);
        Pattern pJson = Pattern.compile("clientId=\"(.*?)\"");
        Matcher matcher = pJson.matcher(res);
        boolean b = matcher.find();
        if (b){
            return matcher.group(1);
        } else {
            throw new IOException("ClientId not found");
        }
    }

    /**
     * 获取直播间的信号源与token参数部分
     * @param rid
     * @return
     * @throws IOException
     */
    private String[] getSigToken(String rid) throws IOException {
        String clientId = getClientId(rid);
        String url = "https://gql.twitch.tv/gql";
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("operationName", "PlaybackAccessToken_Template");
        requestBody.put("query", "query PlaybackAccessToken_Template($login: String!, $isLive: Boolean!, $vodID: ID!, "
                + "$isVod: Boolean!, $playerType: String!) {  streamPlaybackAccessToken(channelName: $login, "
                + "params: {platform: \"web\", playerBackend: \"mediaplayer\", playerType: $playerType}) @include("
                + "if: $isLive) {    value    signature    __typename  }  videoPlaybackAccessToken(id: $vodID, "
                + "params: {platform: \"web\", playerBackend: \"mediaplayer\", playerType: $playerType}) @include("
                + "if: $isVod) {    value    signature    __typename  }}");
        Map<String, Object> variables = new HashMap<>();
        variables.put("isLive", true);
        variables.put("login", rid);
        variables.put("isVod", false);
        variables.put("vodID", "");
        variables.put("playerType", "site");
        requestBody.put("variables", variables);
        Map<String, String> headers = new HashMap<>();

        System.out.println(JSONObject.toJSONString(requestBody));

        /* 使用代理获取链接====start======*/
        String gateIp = "milan.wuyadesiji.eu.org";
        int gatePort = 29775;
        Proxy proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(gateIp, gatePort));
        java.net.Authenticator.setDefault(new java.net.Authenticator() {
            private PasswordAuthentication authentication =
                    new PasswordAuthentication("8Y4yCZ5Olu", "FYl1ve4B94".toCharArray());

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return authentication;
            }
        });
        OkHttpClient client = new OkHttpClient().newBuilder().proxy(proxy).build();
        /* 使用代理获取链接=======end========*/

//        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSONObject.toJSONString(requestBody), MediaType.parse("application/json"));
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Client-ID", clientId)
                .addHeader("Referer", "https://www.twitch.tv/")
                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36")
                .post(body)
                .build();
        Response response = null;
        String responseBody = "";
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                responseBody = response.body().string();
                System.out.println("Response: " + responseBody);
            } else {
                System.out.println("Request failed");
            }
        } catch (IOException e){
            System.out.println("请求失败");
            e.printStackTrace();
        } finally {
            response.close();
        }

        JSONObject jsonObject = JSON.parseObject(responseBody);

        // 获取 data 字段下的 streamPlaybackAccessToken 对象
        JSONObject streamPlaybackAccessToken = jsonObject.getJSONObject("data").getJSONObject("streamPlaybackAccessToken");

        if (streamPlaybackAccessToken != null){
            // 获取 signature 字段的值
            String signature = streamPlaybackAccessToken.getString("signature");
            String token = streamPlaybackAccessToken.getString("value");
            String[] signToken = new String[2];
            signToken[0] = signature;
            signToken[1] = token;
            return signToken;
        } else {
            throw new IOException("Channel does not exist");
        }
    }

    /**
     * 将 Map 对象转换为 URL 查询字符串
     * @param params map
     * @return
     */
    private String encodeParams(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            try {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                sb.append("=");
                sb.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                // 处理编码异常
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
